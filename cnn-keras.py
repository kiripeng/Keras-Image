import os
#图像读取库
from PIL import Image
#矩阵运算库
import numpy as np
import keras
from keras.layers import Dense,Dropout,Flatten
from sklearn.preprocessing import LabelEncoder
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense

data_dir = "data"
test_dir = "exa"

def read_data(data_dir):
    datas = []
    labels = []
    fpaths = []
    datab  = []
    for fname in os.listdir(data_dir):
        fpath = os.path.join(data_dir, fname)
        fpaths.append(fpath)
        image = Image.open(fpath)
        data = np.array(image) / 255.0
        label = int(fname.split("_")[0])
        datas.append(data)
        labels.append(label)


    datas = np.array(datas)
    labels = np.array(labels)

    print("shape of datas: {}\tshape of labels: {}".format(datas.shape, labels.shape))
    return fpaths, datas, labels

fpaths, datas, labels = read_data(data_dir)

num_classes = len(set(labels))

datas.astype(float)
encoder = LabelEncoder()
encoded_Y = encoder.fit_transform(labels)
dummy_y = np_utils.to_categorical(encoded_Y)

input_shape = datas.shape[1:]

_ , test_data, test_label = read_data(test_dir)

test_data.astype(float)
encoder1 = LabelEncoder()
encoded_Y1 = encoder.fit_transform(test_label)
dummy_y1 = np_utils.to_categorical(encoded_Y1)

def baseline_model():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), input_shape=input_shape))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))

    model.add(Flatten())
    model.add(Dense(64))#64
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))  # 3分类
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy',  # 多分类
                 optimizer='rmsprop',
                 metrics=['accuracy'])
    return model



model = baseline_model()

model.fit(datas,dummy_y,
          batch_size=32,
          epochs=100)
scores = model.evaluate(test_data, dummy_y1, verbose=1)

print('Test loss:', scores[0])
print('Test accuracy:', scores[1])

p = model.predict(datas)
model.save('model.h5')
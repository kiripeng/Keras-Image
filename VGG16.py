import os
import numpy as np
from sklearn.preprocessing import LabelEncoder
import tensorflow as tf
import random
import seaborn as sns
import matplotlib.pyplot as plt

from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Flatten, Input
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.optimizers import RMSprop, Adam, SGD
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.applications.vgg16 import VGG16, preprocess_input
from sklearn.model_selection import train_test_split
from PIL import Image

train_dir = 'data'
test_dir = 'exa'

def read_data(data_dir):
    datas = []
    labels = []
    fpaths = []

    for fname in os.listdir(data_dir):
        fpath = os.path.join(data_dir, fname)
        fpaths.append(fpath)
        image = Image.open(fpath)
        data = np.array(image) / 255.0
        label = int(fname.split("_")[0])
        datas.append(data)
        labels.append(label)


    datas = np.array(datas)
    labels = np.array(labels)

    print("shape of datas: {}\tshape of labels: {}".format(datas.shape, labels.shape))
    return fpaths, datas, labels



def vgg16_model():
    vgg16 = VGG16(include_top=False, weights='imagenet', input_shape=input_shape)

    for layer in vgg16.layers:
        layer.trainable = False
    last = vgg16.output
    # 后面加入自己的模型
    x = Flatten()(last)
    x = Dense(2048, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(num_classes, activation='softmax')(x)

    model = Model(inputs=vgg16.input, outputs=x)

    return model



if __name__ == "__main__":

    fpaths, datas, labels = read_data(train_dir)

    num_classes = len(set(labels))

    datas.astype(float)

    encoder = LabelEncoder()
    encoded_Y = encoder.fit_transform(labels)
    dummy_y = np_utils.to_categorical(encoded_Y)

    input_shape = datas.shape[1:]

    _, test_data, test_label = read_data(test_dir)

    test_data.astype(float)
    encoder1 = LabelEncoder()
    encoded_Y1 = encoder.fit_transform(test_label)
    dummy_y1 = np_utils.to_categorical(encoded_Y1)

    model_vgg16 = vgg16_model()
    model_vgg16.summary()
    model_vgg16.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])

    history = model_vgg16.fit(datas, dummy_y, validation_data=(test_data, dummy_y1), epochs=100, batch_size=100, verbose=True)
    score = model_vgg16.evaluate(test_data, dummy_y1, verbose=0)
    print("Large CNN Error: %.2f%%" % (100 - score[1] * 100))
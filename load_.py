from keras.models import load_model
import os
from PIL import Image
from sklearn.preprocessing import LabelEncoder
import numpy as np
from keras.utils import np_utils

def read_data(data_dir):
    datas = []
    labels = []
    fpaths = []

    for fname in os.listdir(data_dir):
        fpath = os.path.join(data_dir, fname)
        fpaths.append(fpath)
        image = Image.open(fpath)
        data = np.array(image) / 255.0
        label = int(fname.split("_")[0])
        datas.append(data)
        labels.append(label)


    datas = np.array(datas)
    labels = np.array(labels)

    print("shape of datas: {}\tshape of labels: {}".format(datas.shape, labels.shape))
    return fpaths, datas, labels


model = load_model('model.h5')
#model.summary()
test_dir = 'exa'
_ , test_data, test_label = read_data(test_dir)

test_data.astype(float)
encoder1 = LabelEncoder()
encoded_Y1 = encoder1.fit_transform(test_label)
dummy_y1 = np_utils.to_categorical(encoded_Y1)

scores = model.evaluate(test_data, dummy_y1, verbose=1)

print('Test loss:', scores[0])
print('Test accuracy:', scores[1])

'''
image = Image.open(fpath)
data = np.array(image) / 255.0
data1 = np.array([data])
p = model.predict(data1)
print(p)
'''

